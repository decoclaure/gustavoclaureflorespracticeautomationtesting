package utils;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

public class SeleniumDriver {

	// initialize webdriver
	private static WebDriver driver;

	// initialize timeouts
	private static WebDriverWait waitDriver;
	public final static int TIMEOUT = 30;
	public final static int PAGE_LOAD_TIMEOUT = 50;

	public static void openPage(String url) {

		System.setProperty("webdriver.chrome.driver",
				System.getProperty("user.dir") + "/src/main/java/utils/chromedriver");
		driver = new ChromeDriver();
		driver.get(url);
		//driver.manage().window().maximize();
		
		waitDriver = new WebDriverWait(driver, TIMEOUT);
		driver.manage().timeouts().implicitlyWait(TIMEOUT, TimeUnit.SECONDS);
		driver.manage().timeouts().pageLoadTimeout(PAGE_LOAD_TIMEOUT, TimeUnit.SECONDS);
		
		
	}

	public static WebDriver getDriver() {
		return driver;
	}
	

}
