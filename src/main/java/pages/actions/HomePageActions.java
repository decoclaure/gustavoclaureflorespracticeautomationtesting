package pages.actions;


import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.asserts.Assertion;
import utils.SeleniumDriver;

public class HomePageActions {

	// move to button and verification
	public void moveToButtonBestSellers() throws InterruptedException {

		// scroll down
		JavascriptExecutor jsx = (JavascriptExecutor) SeleniumDriver.getDriver();
		jsx.executeScript("window.scrollBy(0,720)", "");
		Thread.sleep(2000);

	}

	// 2. Click on “BEST SELLERS”
	public void clickButtonBestSellers() throws InterruptedException {

		SeleniumDriver.getDriver().findElement(By.xpath("//*[@id=\"home-page-tabs\"]/li[2]/a")).click();
	}

	// 3. Verify that “Printed Chiffon Dress” has a discount of 20%
	public void verifyPrintedChiffonDress(String clothing, String discount) {

		WebElement clothin = SeleniumDriver.getDriver()
				.findElement(By.xpath("//*[@id=\"blockbestsellers\"]/li[1]/div/div[2]/h5/a"));
		WebElement discou = SeleniumDriver.getDriver()
				.findElement(By.xpath("//*[@id=\"blockbestsellers\"]/li[1]/div/div[2]/div[1]/span[3]"));
		System.out.println(clothing + " <----------> " + clothin.getText());
		System.out.println(discount + " <----------> " + discou.getText());
		Assertion assertion = new Assertion();

		// verification "Printed Chiffon Dress" and discount
		if (!clothin.getText().equals(clothing)) {
			assertion.assertFalse(false);
		}
		if (!discou.getText().equals(discount)) {
			assertion.assertFalse(false);
		}

	}

	// 4. Hover “Printed Chiffon Dress” and click on “Add to cart"
	public void addCartPrintedChiffonDress() throws InterruptedException {
		WebElement locator = SeleniumDriver.getDriver()
				.findElement(By.xpath("//*[@id=\"blockbestsellers\"]/li[1]/div/div[1]/div/a[1]/img"));
		Actions action = new Actions(SeleniumDriver.getDriver());
		action.moveToElement(locator).perform();
		Thread.sleep(2000);

		SeleniumDriver.getDriver()
				.findElement(By.xpath("//*[@id=\"blockbestsellers\"]/li[1]/div/div[2]/div[2]/a[1]/span")).click();

	}

	// 5 and 7. Click on “Continue shopping”
	public void buttonContinueShoping() throws InterruptedException {
		Thread.sleep(3000);
		SeleniumDriver.getDriver().findElement(By.xpath("//*[@id=\"layer_cart\"]/div[1]/div[2]/div[4]/span/span"))
				.click();
	}

	// 6. Hover “Blouse” and add to the cart.
	public void addBlouseToCart() throws InterruptedException {
		WebElement locator = SeleniumDriver.getDriver()
				.findElement(By.xpath("//*[@id=\"blockbestsellers\"]/li[3]/div/div[1]/div/a[1]/img"));
		Actions action = new Actions(SeleniumDriver.getDriver());
		action.moveToElement(locator).perform();
		Thread.sleep(2000);

		SeleniumDriver.getDriver()
				.findElement(By.xpath("//*[@id=\"blockbestsellers\"]/li[3]/div/div[2]/div[2]/a[1]/span")).click();
	}

	// 8. Go to the “Cart” dropdown and select “Checkout"
	public void cartCheckout() throws InterruptedException {

		// scroll up
		JavascriptExecutor jsx = (JavascriptExecutor) SeleniumDriver.getDriver();
		jsx.executeScript("window.scrollBy(720,0)", "");
		Thread.sleep(2000);

		WebElement locator = SeleniumDriver.getDriver()
				.findElement(By.xpath("//*[@id=\"header\"]/div[3]/div/div/div[3]/div/a"));
		Actions action = new Actions(SeleniumDriver.getDriver());
		action.moveToElement(locator).perform();

		Thread.sleep(1000);
		SeleniumDriver.getDriver().findElement(By.xpath("//*[@id=\"button_order_cart\"]/span")).click();

	}

	// 9.Verify that “Printed Chiffon Dress” and “Blouse” are displayed in the
	// summary table and that the availability displays “In stock”
	public void CheckClothingDisplayed(String printed, String blousep) throws InterruptedException {
		Assertion assertion = new Assertion();
		// scroll dawn
		JavascriptExecutor jsx = (JavascriptExecutor) SeleniumDriver.getDriver();
		jsx.executeScript("window.scrollBy(0,400)", "");
		Thread.sleep(2000);

		// chekin clothing
		WebElement printedChiffonDress = SeleniumDriver.getDriver()
				.findElement(By.xpath("//*[@id=\"product_7_34_0_0\"]/td[2]/p"));
		printedChiffonDress.isDisplayed();
		if (!printedChiffonDress.getText().equals(printed)) {
			assertion.assertFalse(false);
		}
		WebElement inStock1 = SeleniumDriver.getDriver()
				.findElement(By.xpath("//*[@id=\"product_7_34_0_0\"]/td[3]/span"));
		inStock1.isDisplayed();

		WebElement blouse = SeleniumDriver.getDriver().findElement(By.xpath("//*[@id=\"product_7_34_0_0\"]/td[2]/p"));
		blouse.isDisplayed();
		if (!blouse.getText().equals(blousep)) {
			assertion.assertFalse(false);
		}
		WebElement inStock2 = SeleniumDriver.getDriver()
				.findElement(By.xpath("//*[@id=\"product_2_7_0_0\"]/td[3]/span"));
		inStock2.isDisplayed();
	}

//10. Remove “Blouse” from the list
	public void removeBlouse() throws InterruptedException {
		SeleniumDriver.getDriver().findElement(By.xpath("//*[@id=\"2_7_0_0\"]/i")).click();
		Thread.sleep(4000);
	}

	// 11. Verify that “Blouse” is no longer displayed in the table
	public void verifityDisplayedBlouse() {
		Assertion assertion = new Assertion();
		// assertion.assertTrue(condition);

		try {
			SeleniumDriver.getDriver().findElement(By.xpath("//*[@id=\"product_2_7_0_0\"]/td[2]/p/a")).isDisplayed();
			assertion.assertFalse(false, "Failed");
		} catch (Exception e) {
			System.out.println("Blouse is not Displayed");
		}

	}

	// 12. Verify that the total amount to pay is “$18.40"
	public void checkAmountTotal(String amount) {
		Assertion assertion = new Assertion();
		WebElement totalAmount = SeleniumDriver.getDriver().findElement(By.xpath("//*[@id=\"total_price\"]"));
		totalAmount.isDisplayed();
		if (totalAmount.getText().equals(amount)) {
			System.out.println("both amounts are equal => " + totalAmount);
		} else {
			assertion.assertFalse(false, "Test Failed");
		}

	}

	public void driverClosed() {
		SeleniumDriver.getDriver().close();
		System.out.println("Test Finished");
	}
}
