@Checking-Cart
Feature: The test is to verify that the garments are added and removed in the cart

  @Checking-Cart-Positive
  Scenario: Validate Search Cars Page
  
    Given go to "http://automationpractice.com/index.php"
    When I move to button "BEST SELLERS"
    And click on "BEST SELLERS" button
    And Verify that "Printed Chiffon Dress" has a discount of "-20%"
    And Hover "Printed Chiffon Dress" and click on "Add to cart"
    And Click on "Continue shopping"
    And Hover "Blouse" and "Add to the cart"
    And Click on "Continue" shopping
    And Go to the "Cart" dropdown and select "Checkout"
    And Verify that "Printed Chiffon Dress" and "Blouse" are displayed in the summary table and that the availability displays "In stock"
    And Remove "Blouse" from the list
    And Verify that "Blouse" is no longer displayed in the table
    And Verify that the total amount to pay is "$18.40"
