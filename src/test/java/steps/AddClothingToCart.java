package steps;



import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import pages.actions.HomePageActions;
import utils.SeleniumDriver;

public class AddClothingToCart {

	HomePageActions homePageActions = new HomePageActions();

	// 1. Go to: hhttp://automationpractice.com/index.php
	@Given("^go to \"([^\"]*)\"$")
	public void go_to(String websiteURL) throws Throwable {

		System.out.println("1. Go to: " + websiteURL);
		SeleniumDriver.openPage(websiteURL);
	}

	// move to button
	@When("^I move to button \"([^\"]*)\"$")
	public void i_move_to_button(String button) throws Throwable {
		System.out.println("move to button " + button);
		Thread.sleep(3000);
		homePageActions.moveToButtonBestSellers();

	}

	@When("^click on \"([^\"]*)\" button$")
	public void click_on_button(String button) throws Throwable {
		System.out.println("2. Click on " + button);
		homePageActions.clickButtonBestSellers();
	}

	@When("^Verify that \"([^\"]*)\" has a discount of \"([^\"]*)\"$")
	public void verify_that_has_a_discount_of(String clothing, String discount) throws Throwable {
		System.out.println("3. Verify that “Printed Chiffon Dress” has a discount of 20%");
		homePageActions.verifyPrintedChiffonDress(clothing, discount);
	}

	@When("^Hover \"([^\"]*)\" and click on \"([^\"]*)\"$")
	public void hover_and_click_on(String clothing, String cartButtun) throws Throwable {
		System.out.println("Hover " + clothing + " and click on " + cartButtun);

		homePageActions.addCartPrintedChiffonDress();
	}

	@When("^Click on \"([^\"]*)\"$")
	public void click_on(String arg1) throws Throwable {
		System.out.println("click to button " + arg1);

		homePageActions.buttonContinueShoping();
	}

	@When("^Hover \"([^\"]*)\" and \"([^\"]*)\"$")
	public void hover_and(String blouse, String cart) throws Throwable {
		System.out.println("Hover " + blouse + " and add to the " + cart);
		homePageActions.addBlouseToCart();
	}

	@When("^Click on \"([^\"]*)\" shopping$")
	public void click_on_shopping(String buttonContnue) throws Throwable {
		System.out.println("Click on " + buttonContnue + " shopping");
		homePageActions.buttonContinueShoping();
	}

	@When("^Go to the \"([^\"]*)\" dropdown and select \"([^\"]*)\"$")
	public void go_to_the_dropdown_and_select(String cart, String checkout) throws Throwable {
		System.out.println("Go to the " + cart + " dropdown and select " + checkout);
		homePageActions.cartCheckout();
	}

	@When("^Verify that \"([^\"]*)\" and \"([^\"]*)\" are displayed in the summary table and that the availability displays \"([^\"]*)\"$")
	public void verify_that_and_are_displayed_in_the_summary_table_and_that_the_availability_displays(String printed,
			String blouse, String inStock) throws Throwable {
		System.out.println("Verify that " + printed + " and " + blouse
				+ " are displayed in the summary table and that the availability displays" + inStock);
		homePageActions.CheckClothingDisplayed(printed, blouse);
	}

	@When("^Remove \"([^\"]*)\" from the list$")
	public void remove_from_the_list(String blouse) throws Throwable {
		System.out.println("Remove " + blouse + " from the list");
		homePageActions.removeBlouse();
	}

	@When("^Verify that \"([^\"]*)\" is no longer displayed in the table$")
	public void verify_that_is_no_longer_displayed_in_the_table(String blouse) throws Throwable {
		System.out.println("Verify that " + blouse + " is no longer displayed in the table");
		homePageActions.verifityDisplayedBlouse();
	}

	@When("^Verify that the total amount to pay is \"([^\"]*)\"$")
	public void verify_that_the_total_amount_to_pay_is(String amount) throws Throwable {
		System.out.println("Verify that the total amount to pay is " + amount);
		homePageActions.checkAmountTotal(amount);
		Thread.sleep(10000);
		homePageActions.driverClosed();

	}

}
